//********************************************************************
//Configuration of Dialog System
//********************************************************************
/*
    To create menus, notecard _DialogConfig should exist in the format:
    
        *MENU NAME
        "Displayed Menu Title
        ButtonName=ButtonAction
    
    Current supported actions are:
        MENU|((name))   display a menu from the notecard
        LINKMSG|((link message))    send a link message or everything following LINKMSG|
    
    
    To generate an ad-hoc menu from a link message:
    
    Send the link message with action DIALOG, followed by title, and a list of buttons,
    finally the "return key" you would like to specify.
    
    Button list format should substitute ~ for |, during this message, example:
        llMessageLinked(LINK_SET, 0, "DIALOG|This is a test.|ONE=LINKMSG~JOIN~{ME},TWO=MENU~MAIN|MULTIPLAYER", "");

*/

integer DEBUG=TRUE;

// CONFIG VARS
string cConfigurationNotecardName = "_DialogConfig";
key cNotecardQueryId;
integer cLine;
string cCurrentMenu;

// DIALOG VARS
integer gMenuReady;
integer gChanDialog;
integer gHandleDialog;
key gUser;
string gMenus;
string gCurrentMenu;
integer gMenuInUse;
list gReplacements;

list gTempButtons;
list gTempActions;

generateReplacements() {
    gReplacements = [
        "{ME}", (string)llGetOwner(),
        ">>", "\n"
    ];
}

debug(string message) {
    if(DEBUG)
        llOwnerSay(message);   
}

string parseReplacements(string message) {
    integer n;
    for(n=0; n<llGetListLength(gReplacements); n=n+2) {
        message = strReplace(message, llList2String(gReplacements, n), llList2String(gReplacements, n+1));   
    }
    return message;
}

parseAndSend(string message) {
    llMessageLinked(LINK_SET, ACTIVE, parseReplacements(message), (key)gCurrentMenu);    
}

string strReplace(string str, string search, string replace) {
    return llDumpList2String(llParseStringKeepNulls((str = "") + str, [search], []), replace);
}

init() {
    generateReplacements();
    gMenuReady = FALSE;
    gChanDialog = -1 - (integer)("0x" + llGetSubString( (string) llGetKey(), -7, -1) );

    if(llGetInventoryType(cConfigurationNotecardName) != INVENTORY_NOTECARD) {
        llOwnerSay("Missing inventory notecard: " + cConfigurationNotecardName);
        return;
    }
    
    cLine = 0;
    cNotecardQueryId = llGetNotecardLine(cConfigurationNotecardName, cLine);
}

ProcessConfiguration(string data) {
    if(data == EOF) {
        llOwnerSay("Dialog System Ready.");
        gMenuReady = TRUE;
        return;
    }
    
    if(data != "") {
        if(llSubStringIndex(data, "#") != 0) {
            
            if(llGetSubString(data, 0, 0) == "*") {
                // New Menu
                cCurrentMenu = llGetSubString(data, 1, -1);
                gMenus = llJsonSetValue(gMenus, [cCurrentMenu], "");
            }
            
            if(llGetSubString(data, 0, 0) == "\"") {
                // Menu Text
                gMenus = llJsonSetValue(gMenus, [cCurrentMenu, "title"], llGetSubString(data, 1, -1));
            }
            
            integer index = llSubStringIndex(data, "=");
            if(index != -1) {
                // Button definition
                string buttonName = llGetSubString(data, 0, index-1);
                string buttonAction = llGetSubString(data, index+1, -1);
                gMenus = llJsonSetValue(gMenus, [cCurrentMenu, "buttons", buttonName], buttonAction);
            }

        }
    }
    cNotecardQueryId = llGetNotecardLine(cConfigurationNotecardName, ++cLine);
}

generateMenu(string payload) {
    list temp = llParseString2List(payload, ["|"], []);
    string title = llList2String(temp, 0);
    list buttonsAndActions = llParseString2List(llList2String(temp, 1), [","], []);
    string returnKey = llList2String(temp, 2);
    
    integer n;
    gTempButtons = [];
    gTempActions = [];
    for(n=0; n<llGetListLength(buttonsAndActions); n++) {
        list btemp = llParseString2List(llList2String(buttonsAndActions, n), ["="], []);
        gTempButtons += llList2String(btemp, 0);
        gTempActions += strReplace(llList2String(btemp, 1), "~", "|");
    }
    gHandleDialog = llListen(gChanDialog+1, "", "", "");
    gCurrentMenu = returnKey;
    
    llDialog(gUser, title, gTempButtons, gChanDialog+1);
    llSetTimerEvent(30);
}

showMenu(string menu) {
    gHandleDialog = llListen(gChanDialog, "", "", "");
    gCurrentMenu = menu;
    string title = llJsonGetValue(gMenus, [menu, "title"]);
    string buttonString = llJsonGetValue(gMenus, [menu, "buttons"]);
    list buttons = llJson2List(buttonString);
    
    // Newlines in JSON aren't parsed, so handle them now
    title = parseReplacements(title);
    
    // Buttons becomes a 2-strided list, we only want the button name stride
    buttons = llList2ListStrided(buttons, 0, -1, 2);
    
    llDialog(gUser, title, buttons, gChanDialog);
    llSetTimerEvent(30);
}

processPayload(string action) {
    list parsed = llParseString2List(action, ["|"], []);
    string action = llList2String(parsed, 0);
    string payload = llDumpList2String(llDeleteSubList(parsed, 0, 0), "|");
    
    if(action == "MENU") {
        showMenu(payload);   
    }
    if(action == "LINKMSG") {
        parseAndSend(payload);
    }
    if(action == "DIALOG") {
        generateMenu(payload);
    }
}

messageToAction(string button) {
    string buttonAction = llJsonGetValue(gMenus, [gCurrentMenu, "buttons", button]);
    processPayload(buttonAction);
    llSetTimerEvent(30);
    
}

tempMessageToAction(string button) {
    integer buttonIndex = llListFindList(gTempButtons, [button]);
    string buttonAction = llList2String(gTempActions, buttonIndex);
    processPayload(buttonAction);
    llSetTimerEvent(30);
    
}

default {
    
    state_entry() {
        init();
    }
    
    touch_start(integer total_number) {
        // Determining if menu is in use is probably not necessary once these
        // aren't boxes sitting on a desk   
        if (gMenuInUse == FALSE && gMenuReady) {
            gUser = llDetectedKey(0);
            gMenuInUse = TRUE;
            showMenu("MAIN");
            llSetTimerEvent(30);
        } else if (gUser == llDetectedKey(0) && gMenuReady) {
            showMenu("MAIN");
            llSetTimerEvent(30);
        }
    }
    
    listen(integer channel, string name, key id, string message) {
        if (channel == gChanDialog) {
            messageToAction(message);
        }
        if (channel == gChanDialog+1) {
            tempMessageToAction(message);
        }
    }

    changed(integer change) {
        if(change & (CHANGED_OWNER | CHANGED_INVENTORY)) {
            init();
        }
    }

    dataserver(key request_id, string data) {
        if(request_id == cNotecardQueryId)
            ProcessConfiguration(data);
    }

    timer() {
        gMenuInUse = FALSE;
        llListenRemove(gHandleDialog);
    }

    link_message(integer sender_num, integer num, string message, key id) {
        list temp = llParseString2List(message, ["|"], [""]);
        string action = llList2String(temp, 0);
        
        if(action == "DIALOG" || action == "MENU")
            processPayload(message);

    }
}
