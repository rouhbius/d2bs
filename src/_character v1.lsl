string characterStats; //Json format for the character data
integer points = 6; //the number of points you may invest in HD, SD, and additional stats
integer choices_modifier = 2; //The number of base modifiers to arm, tec, pwr, and mvr
key owner; //the user of the menu

integer channel_dialog;
integer listen_handle;

integer hd;
integer sd;
integer hp;
integer sh;
integer ac;
integer arm;
integer mvr;
integer pwr;
integer tec;
integer stat;
integer in_use;


list dialog_statbuff = [0, 6, 8, 10, 12]; //list for the menu
list dialog_statbuff2 = ["+0|-0", "+1|-1", "+2|-1", "+3|-2", "+4|-2"]; //list for the menu
string menu_page;
integer points_stat; //how many points are invested in additional stats
integer points_statAdd; //how many bonus positive points are earned
integer points_statSub; //how many bonus negative points are earned
integer points_sd;  //how many points are invested in sd
integer points_hd; //yada yada yada...
integer error; //if a player chose to add points to a stat which has already been maxed

key gKeyDialog;
key gKeyCharacter;
key gKeyMultiplayer;

init() 
{
    gKeyDialog = llGetInventoryKey("_dialog V2");
    gKeyCharacter = llGetInventoryKey("_Character v1.0");
    gKeyMultiplayer = llGetInventoryKey("_multiplayer V2");
//    owner = llGetOwner(); commented this line for testing purposes
    channel_dialog = -1 - (integer)("0x" + llGetSubString( (string) llGetKey(), -7, -1) ); //create a custom channel for the object

        //after completing the menu, offer a chance to start over
        string reset = llGetObjectDesc();
        if (reset != "") {
            owner = reset;
            llSetObjectDesc("");
            llListenControl(listen_handle, TRUE);
            llSetText("listening", <1.0,1.0,1.0>, 1.0);
            generate_menu();
            llSetTimerEvent(20);
        }
}

listener()
{
    listen_handle = llListen(channel_dialog, "", owner, "");
    llListenControl(listen_handle, TRUE);
//    llSetText("not listening", <0.0,0.0,0.0>, 1.0);
} 

create_character()
{
        //take the menu choice from stat and turn it into something meaningful.
        string stat_choice = llList2String(dialog_statbuff2, points_stat);
        list temp = llParseString2List(stat_choice, ["|", "+", "-"],[""]);
        integer add = llList2Integer(temp,0);
        integer subtract = llList2Integer(temp,1);

        string ship_class = llJsonGetValue(characterStats, ["class"]);
    //talk to Jen about this
    if (mvr < 0) {
        mvr -= subtract;
    }
    if (mvr > 0) {
        mvr += add;
    }

    if (pwr < 0) {
        pwr -= subtract;
    }
    if (pwr > 0) {
        pwr += add;
    }  

    if (arm < 0) {
        arm -= subtract;
    }
    if (arm > 0) {
        arm += add;
    }

    if (tec < 0) {
        tec -= subtract;
    }
    if (tec > 0) {
        tec += add;
    }
    
    if (ship_class == "Cruiser") {
        arm += 2;        
    }

    if (ship_class == "Destroyer") {
        mvr += 1;
        pwr += 1;
    }

    if (ship_class == "Science") {
        tec += 2;
    }
    
    //HP - (HD*10) + (HD*(LVL-1)) + (ARM*LVL) HD*10 First level
    hp = ((integer)hd * 10) + ((integer)hd *(1-1)) + (arm * 1);
    //SH - (SD*10) + (SD*(LVL-1)) + (TEC*LVL) Same calc as HP with TEC
    sh = ((integer)sd * 10) + ((integer)sd *(1-1)) + (tec * 1);
    //AC - (10 + MVR) how difficult it is to hit.
    ac = (10 + mvr);
    characterStats = llJsonSetValue(characterStats, ["lvl"], "1");
    characterStats = llJsonSetValue(characterStats, ["hp"], (string)hp);
    characterStats = llJsonSetValue(characterStats, ["hd"], (string)hd);
    characterStats = llJsonSetValue(characterStats, ["sh"], (string)sh);
    characterStats = llJsonSetValue(characterStats, ["sd"], (string)sd);
    characterStats = llJsonSetValue(characterStats, ["ac"], (string)ac);
    characterStats = llJsonSetValue(characterStats, ["arm"], (string)arm);
    characterStats = llJsonSetValue(characterStats, ["mvr"], (string)mvr);
    characterStats = llJsonSetValue(characterStats, ["pwr"], (string)pwr);
    characterStats = llJsonSetValue(characterStats, ["tec"], (string)tec);
}

generate_menu()
{
    //menu page 1
    if (menu_page == "class") {
        llSetTimerEvent(20);
        llDialog(owner, "choose ship type" + "\n", ["Cruiser", "Destroyer", "Science"], channel_dialog);
    }
    //menu page 2
    if (menu_page == "modifiers") {
        llSetTimerEvent(20);
        llDialog(owner, "::Ship Design::" + "\n" + "begin with 2 stat modifiers (can pick the same twice)" + "\n" + 
        "Modifiers remaining: " + (string)choices_modifier + "\n" +
                "~~~~~~~~~~~~~~~~~~~" + "\n" + "\n" +            
                "PWR -- Power: " + (string)pwr + "\n" +
                "ARM -- Armor: " + (string)arm + "\n" +
                "TEC -- Technology power: " + (string)tec + "\n" +
                "MVR -- Maneuverability " + (string)mvr,
                ["MVR+1, ARM-1", "ARM+1, MVR-1", "PWR+1, TEC-1", "TEC+1, PWR-1"], channel_dialog);


    }
    //menu page 3
    if (menu_page == "points") {
        llSetTimerEvent(20);
        if (!error) {
            string stat_choice = llList2String(dialog_statbuff2, points_stat);
            llDialog(owner, "Further customize by choosing at least one from each of the the following categories:" + "\n" + "\n" +
            "HD -- Base HP gains/level): " + (string)points_hd + "/4" + "\n" +
            "SD -- Base SH gains/level): " + (string)points_sd + "/4" + "\n" +
            "ST -- base Stat gains/level): " + (string)points_stat + "/4" + "\n" +
                "~~~~~~~~~~~~~~~~~~~" + "\n" + "\n" +
                "HD Bonus: " + (string)hd + "\n" +
                "SD Bonus: " + (string)sd + "\n" +
                "Stat Bonus: " + stat_choice + "\n" +
                "~~~~~~~~~~~~~~~~~~~" + "\n" +
                    "Remaining Points: " + (string)points,
                    ["HD", "SD", "Stat"], channel_dialog);
        }
        else {
            error = FALSE;
            string stat_choice = llList2String(dialog_statbuff2, points_stat);
            llDialog(owner, "ERROR! " + "\n" + "Max level already achieved in HD. Please select one of the remaining categories:" + "\n" + 
            "HD -- Base HP gains/level): " + (string)points_hd + "/4" + "\n" +
            "SD -- Base SH gains/level): " + (string)points_sd + "/4" + "\n" +
            "ST -- base Stat gains/level): " + (string)points_stat + "/4" + "\n" +
                "~~~~~~~~~~~~~~~~~~~" + "\n" +
                "HD Bonus: " + (string)hd + "\n" +
                "SD Bonus: " + (string)sd + "\n" +
                "Stat Bonus: " + stat_choice + "\n" +
                "~~~~~~~~~~~~~~~~~~~" + "\n" +
                    "Remaining Points: " + (string)points
                    , ["HD", "SD", "Stat"], channel_dialog);
        }
    }

    //menu page 4
    if (menu_page == "assignPoints") {
        llSetTimerEvent(20);
        if (!error) {
            llDialog(owner, "Assign the bonus positive and negative points:" + "\n" + "\n" +
            "PWR -- Power): " + (string)pwr + "\n" +
            "ARM -- Armor): " + (string)arm + "\n" +
            "TEC -- Technology power): " + (string)tec + "\n" +
            "MVR -- Maneuverability): " + (string)mvr + "\n" +
                "~~~~~~~~~~~~~~~~~~~" + "\n" + "\n" +
                    "Remaining Negative Points " + (string)points_statSub + "\n" +
                    "Remaining POSITIVE Points: " + (string)points_statAdd,
                    ["+PWR", "-PWR", "+ARM", "-ARM", "+TEC", "-TEC", "+MVR", "-MVR"], channel_dialog);
        }
        else {
            error = FALSE;
                llDialog(owner, "Error! " + "\n" +
                "Assign the bonus positive and negative points:" + "\n" + "\n" +
                "PWR -- Power): " + (string)pwr + "\n" +
                "ARM -- Armor): " + (string)arm + "\n" +
                "TEC -- Technology power): " + (string)tec + "\n" +
                "MVR -- Maneuverability): " + (string)mvr + "\n" +
                "~~~~~~~~~~~~~~~~~~~" + "\n" + "\n" +
                    "Remaining Negative Points " + (string)points_statSub + "\n" +
                    "Remaining POSITIVE Points: " + (string)points_statAdd,
                    ["+PWR", "-PWR", "+ARM", "-ARM", "+TEC", "-TEC", "+MVR", "-MVR"], channel_dialog);
            }
}

    if (menu_page == "complete") {
        llDialog(owner, "Begin again?",["Yes", "No"], channel_dialog);
    }
}

assign_class(string message)
{
    characterStats = llJsonSetValue(characterStats, ["class"], message);
    llSetObjectName(message);
}




default
{
    state_entry()
    {
        in_use = FALSE;
        menu_page = "class";
        init();
//        listener();
        llOwnerSay("Free memory: " + (string)llGetFreeMemory());
    }

    touch_start(integer total_number)
    {
        if (in_use == FALSE) {
            in_use = TRUE;
            owner = llDetectedKey(0);
/*
            if (owner) {
                llListenControl(listen_handle, TRUE);
                llSetText("listening", <1.0,1.0,1.0>, 1.0);
                generate_menu();
*/
        }
    }

    timer()
    {
        in_use = FALSE;
        llSay(0, "Menu has timed out.");
        llSetObjectName("Simple ship");
        llSetObjectDesc("");
        llResetScript();
    }

    //receives on 0, sends on 1
    link_message(integer sender_num, integer num, string msg, key id)
    {
        if (num == 0) {
            if (msg == "Create") {
                    in_use = TRUE;
                    listener();
//                    llListenControl(listen_handle, TRUE);
//                    llSetText("listening", <1.0,1.0,1.0>, 1.0);
                    generate_menu();
            }
        }
    }

    listen(integer channel, string name, key id, string message)
    {
        assign_class(message);
        state choose_modifiers;
    }
}

state choose_modifiers
{
    state_entry()
    {
        menu_page = "modifiers";
        listener();
        generate_menu();
    }
/*
    touch_start(integer total_number)
    {
        if (owner) {
            llListenControl(listen_handle, TRUE);
            llSetText("listening", <1.0,1.0,1.0>, 1.0);
            generate_menu();
        }
    }
*/
    timer()
    {
        llSay(0, "Menu has timed out.");
        llResetScript();
    }

    listen(integer channel, string name, key id, string message)
    {
        if (message == "MVR+1, ARM-1") {
            mvr += 1;
            arm -= 1;
            choices_modifier -= 1;
            if (choices_modifier >= 1)
            {
                generate_menu();
            }
            else {
                state choose_points;
            }
        }
        if (message == "ARM+1, MVR-1") {
            mvr -= 1;
            arm += 1;
            choices_modifier -= 1;
            if (choices_modifier > 0)
            {
                generate_menu();
            }
            else {
                state choose_points;
            }
        }
        if (message == "PWR+1, TEC-1") {
            pwr += 1;
            tec -= 1;
            choices_modifier -= 1;
            if (choices_modifier > 0)
            {
                generate_menu();
            }
            else {
                state choose_points;
            }
        }
        if (message == "TEC+1, PWR-1") {
            pwr -= 1;
            tec += 1;
            choices_modifier -= 1;
            if (choices_modifier > 0)
            {
                generate_menu();
            }
            else {
                state choose_points;
            }
        }
    }
}

state choose_points
{
    state_entry()
    {
        menu_page = "points";
        listener();
        generate_menu();
    }
/*
    touch_start(integer total_number)
    {
        if (owner) {
            llListenControl(listen_handle, TRUE);
            llSetText("listening", <1.0,1.0,1.0>, 1.0);
            generate_menu();
        }
    }
*/
    timer()
    {
        llSay(0, "Menu has timed out.");
        llResetScript();
    }

    listen(integer channel, string name, key id, string message)
    {
        if (message == "HD") { //if HD selected
            if ((points -1) == 0) { // if this is our last point
                if (points_hd < 4) { //if our last point can go into HD
                    points_hd += 1;
                    points -= 1;
                    hd = llList2Integer(dialog_statbuff, points_hd);
                        if (points_stat > 0) {
                            state assignPoints; //assign bonus points chosen
                        }
                        else {
                            state complete;
                        }
                }
                else { //if our last point can't go into HD
                    error = TRUE; //we have an error
                    generate_menu(); //create the menu with an error
                }
            }
            else { //if this is not our last point
                if (points_hd < 4) { //if our points in HD are not maxed
                    points_hd += 1;
                    points -= 1;
                    hd = llList2Integer(dialog_statbuff, points_hd);
                    generate_menu(); //return the menu for additional point selection
                }
                else { //if this point cannot go into HD, and we have additional points
                    error = TRUE; //we have an error
                    generate_menu(); //create the menu with an error
                }
            }
        }

        if (message == "SD") { //if HD selected
            if ((points -1) == 0) { // if this is our last point
                if (points_sd < 4) { //if our last point can go into HD
                    points_sd += 1;
                    points -= 1;
                    sd = llList2Integer(dialog_statbuff, points_sd);
                        if (points_stat > 0) {
                            state assignPoints; //assign bonus points chosen
                        }
                        else {
                            state complete; //finish and generate character
                        }
                }
                else { //if our last point can't go into HD
                    error = TRUE; //we have an error
                    generate_menu(); //create the menu with an error
                }
            }
            else { //if this is not our last point
                if (points_sd < 4) { //if our points in HD are not maxed
                    points_sd += 1;
                    points -= 1;
                    sd = llList2Integer(dialog_statbuff, points_sd);
                    generate_menu(); //return the menu for additional point selection
                }
                else { //if this point cannot go into HD, and we have additional points
                    error = TRUE; //we have an error
                    generate_menu(); //create the menu with an error
                }
            }
        }

        if (message == "Stat") { //if HD selected
            if ((points -1) == 0) { // if this is our last point
                if (points_stat < 4) { //if our last point can go into HD
                    points_stat += 1;
                    points -= 1;
                    stat = llList2Integer(dialog_statbuff, points_stat);
                        if (points_stat > 0) {
                            state assignPoints; //assign bonus points chosen
                        }
                        else {
                            state complete; //finish and create the character
                        }
                }
                else { //if our last point can't go into HD
                    error = TRUE; //we have an error
                    generate_menu(); //create the menu with an error
                }
            }
            else { //if this is not our last point
                if (points_stat < 4) { //if our points in HD are not maxed
                    points_stat += 1;
                    points -= 1;
                    stat = llList2Integer(dialog_statbuff, points_stat);
                    generate_menu(); //return the menu for additional point selection
                }
                else { //if this point cannot go into HD, and we have additional points
                    error = TRUE; //we have an error
                    generate_menu(); //create the menu with an error
                }
            }
        }
    }
}

state assignPoints
{
    state_entry()
    {
        menu_page = "assignPoints";
        listener();
        string stat_choice = llList2String(dialog_statbuff2, points_stat);
        list temp = llParseString2List(stat_choice, ["|", "+", "-"],[""]);
        points_statAdd = llList2Integer(temp,0);
        points_statSub = llList2Integer(temp,1);
        generate_menu();
    }
/*
    touch_start(integer total_number)
    {
        if (owner) {
            llListenControl(listen_handle, TRUE);
            llSetText("listening", <1.0,1.0,1.0>, 1.0);
            generate_menu();
        }
    }
*/
    timer()
    {
        llSetObjectName("Simple ship");
        llSetObjectDesc("");
        llResetScript();
    }

    listen(integer channel, string name, key id, string message)
    {
        if (message == "+PWR") {
            if (points_statAdd > 0) { //if we have points remaining
                pwr += 1;
                points_statAdd -= 1;
                    if (points_statSub + points_statAdd > 0) //if there are more points to add or subtract
                    {
                        generate_menu(); //generate another menu
                    }
                    else {
                        state complete;//otherwise, finish and create the character
                    }
            }
            else {
                error = TRUE;
                generate_menu(); //if we do not have points, generate an ERROR menu
            }
        }

        else if (message == "-PWR") {
            if (points_statSub > 0) { //if we have points remaining
                pwr -= 1;
                points_statSub -= 1;
                    if (points_statSub + points_statAdd > 0) //if there are more points to add or subtract
                    {
                        generate_menu(); //generate another menu
                    }
                    else {
                        state complete;//otherwise, finish and create the character
                    }
            }
            else {
                error = TRUE;
                generate_menu(); //if we do not have points, generate an ERROR menu
            }
        }

        else if (message == "+ARM") {
            if (points_statAdd > 0) { //if we have points remaining
                arm += 1;
                points_statAdd -= 1;
                    if (points_statSub + points_statAdd > 0) //if there are more points to add or subtract
                    {
                        generate_menu(); //generate another menu
                    }
                    else {
                        state complete;//otherwise, finish and create the character
                    }
            }
            else {
                error = TRUE;
                generate_menu(); //if we do not have points, generate an ERROR menu
            }
        }

        else if (message == "-ARM") {
            if (points_statSub > 0) { //if we have points remaining
                arm -= 1;
                points_statSub -= 1;
                    if (points_statSub + points_statAdd > 0) //if there are more points to add or subtract
                    {
                        generate_menu(); //generate another menu
                    }
                    else {
                        state complete;//otherwise, finish and create the character
                    }
            }
            else {
                error = TRUE;
                generate_menu(); //if we do not have points, generate an ERROR menu
            }
        }

        else if (message == "+TEC") {
            if (points_statAdd > 0) { //if we have points remaining
                tec += 1;
                points_statAdd -= 1;
                    if (points_statSub + points_statAdd > 0) //if there are more points to add or subtract
                    {
                        generate_menu(); //generate another menu
                    }
                    else {
                        state complete;//otherwise, finish and create the character
                    }
            }
            else {
                error = TRUE;
                generate_menu(); //if we do not have points, generate an ERROR menu
            }
        }

        else if (message == "-TEC") {
            if (points_statSub > 0) { //if we have points remaining
                tec -= 1;
                points_statSub -= 1;
                    if (points_statSub + points_statAdd > 0) //if there are more points to add or subtract
                    {
                        generate_menu(); //generate another menu
                    }
                    else {
                        state complete;//otherwise, finish and create the character
                    }
            }
            else {
                error = TRUE;
                generate_menu(); //if we do not have points, generate an ERROR menu
            }
        }

        else if (message == "+MVR") {
            if (points_statAdd > 0) { //if we have points remaining
                mvr += 1;
                points_statAdd -= 1;
                    if (points_statSub + points_statAdd > 0) //if there are more points to add or subtract
                    {
                        generate_menu(); //generate another menu
                    }
                    else {
                        state complete;//otherwise, finish and create the character
                    }
            }
            else {
                error = TRUE;
                generate_menu(); //if we do not have points, generate an ERROR menu
            }
        }

        else if (message == "-MVR") {
            if (points_statSub > 0) { //if we have points remaining
                mvr -= 1;
                points_statSub -= 1;
                    if (points_statSub + points_statAdd > 0) //if there are more points to add or subtract
                    {
                        generate_menu(); //generate another menu
                    }
                    else {
                        state complete;//otherwise, finish and create the character
                    }
            }
            else {
                error = TRUE;
                generate_menu(); //if we do not have points, generate an ERROR menu
            }
        }
    }
}

state complete
{
    state_entry()
    {
        menu_page = "complete";
        listener();
        create_character();
        llSay(0,"Character creation complete. Output:" + "\n" + characterStats);
        llSetTimerEvent(20);
    }
/*
    touch_start(integer total_number)
    {
        if (owner) {
            llListenControl(listen_handle, TRUE);
            llSetText("listening", <1.0,1.0,1.0>, 1.0);
            generate_menu();
        }
    }
*/
    timer()
    {
        in_use = FALSE;
        llSetObjectName("Simple ship");
        llSetObjectDesc("");
        llResetScript();
    }
/*
    listen(integer channel, string name, key id, string message)
    {
        if (message == "Yes") {
            llSetObjectName("Simple ship");
            llSetObjectDesc(owner);
            llResetScript();
        }
        else {
            llSetObjectName("Simple ship");
            llSetObjectDesc("");
            llResetScript();
        }
    }
*/
}