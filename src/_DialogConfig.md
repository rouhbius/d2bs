# Start line with * to begin new menu
# Start line with " to create menu title text
# Use >> instead of \n for newlines in title text
# Known actions:
# MENU:[menuname] Displays the chosen menu
# LINKMSG:[string] Sends a link message with the chosen string, id set to current menu name

*MAIN
"This is the Main Menu>>Please make a selection below.
Character=MENU|CHARACTER
Multiplayer=MENU|MULTIPLAYER

*CHARACTER
"Character Creation Menu>>Let's create a character!
Summary=MENU|CHARACTER
Create=LINKMSG|Create

*MULTIPLAYER
"Multiplayer Menu>>Time for some fun!
Host Match=LINKMSG|Host Match|{ME}
Join Match=LINKMSG|Join Match|Whatever|Whatever2|Whatever3