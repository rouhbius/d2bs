/////////////////////////
// multiplayer code    //
// Version 1.0         //
// Apr 2 2019          //
/////////////////////////


//**************************************************************************
//Set up channels for global and match communication
//**************************************************************************
integer gChanGlobal;
integer gHandleGlobal;

integer gChanMatch;
integer gHandleMatch;

integer gChanQuery;
integer gHandleQuery;


//**************************************************************************
//Set up unique keys for script communication via linked messages
//**************************************************************************
string gDialog = "DIALOG";
string gMultiplayer = "MULTIPLAYER";
key gOwner;
key gHost; 


//**************************************************************************
//Set up variables for multiplayer matches
//**************************************************************************

integer gClientNum;
integer gClientMax;
integer gJoinedMatch; //TRUE OR FALSE
integer gQuery; //TRUE or FALSE
list gPlayerList;
list gPlayer1Data;
list gPlayer2Data;
list gPlayer3Data;
list gPlayer4Data;
list gPlayer5Data;
list gMatchList;


//**************************************************************************
//Set up custom functions
//**************************************************************************

init() {
    gOwner = llGetOwner();
    gChanMatch = -1 - (integer)((llFrand(1000) + (llFrand(1000) / 2)));

    gChanGlobal = -141241255;
    gChanQuery = -1 - (integer)("0x" + llGetSubString( (string)gOwner, -6, -1) );

    gClientNum = 0;
    gClientMax = 4;

    gPlayerList = [];
    gPlayer1Data = [];
    gPlayer2Data = [];
    gPlayer3Data = [];
    gPlayer4Data = [];
    gPlayer5Data = [];
}

default
{
    state_entry()
    {
        init();
    }

    link_message(integer source, integer num, string str, key id)
    {
        list temp = llParseString2List(str, ["|"], [""]);
        string action = llList2String(temp, 0);
        if (action == "Host Match") {
            state host_setup;
        } else if (action == "Join Match") {
            state client_setup;
        }
    }
}

state host_setup
{
    state_entry()
    {
        gChanMatch = -1 - (integer)("0x" + llGetSubString( (string)gOwner, -7, -1) );
        gHandleGlobal = llListen(gChanGlobal, "", "", "");
        gHandleMatch = llListen(gChanMatch, "", "", "");
        gPlayerList = gPlayerList + [gOwner];
        gPlayer1Data = [gOwner];
        gJoinedMatch = TRUE;
        llSay(0, llKey2Name(gOwner) + " is hosting on " + (string)gChanMatch + " channel, with " + (string)gClientNum + "/" + (string)gClientMax + " clients, and is ready for client connections.");
    }

    listen(integer chan, string name, key id, string message)
    {
        if (chan == gChanMatch) {
            list temp = llParseString2List(message, ["|"], [""]);
            string action = llList2String(temp, 0);
            string body = llList2String(temp, 1);

            if (action == "JOIN") {
                integer i = llListFindList(gPlayerList, [body]);
                if (i == -1) {
                    if (gClientNum < gClientMax) {
                        gClientNum += 1;
                        gPlayerList = gPlayerList + [body];
                        integer chanResponse = llList2Integer(temp, 2);
                        llRegionSay(chanResponse,"WELCOME|" + (string)gOwner + "|" + (string)gChanMatch);
                        llSay(0, "Adding Player " + "secondlife:///app/agent/" + (string)body + "/username");
                    } else {
                        llSay(0, "Error: Maximum number of clients has been reached.");
                    }
                } else {
                    llSay(0, "Player " +"secondlife:///app/agent/" + (string)body + "/username" + " is already a player in this match.");
                }
            } else if (action == "LEAVE") {
                integer i = llListFindList(gPlayerList, [body]);
                if (i != -1) {
                    if (body != gOwner) {
                        gClientNum -= 1;
                        llSay(0, "Player " + "secondlife:///app/agent/" + (string)llList2String(gPlayerList, i) + "/username" + " removed.");
                        gPlayerList = llDeleteSubList(gPlayerList, i, i);
                    } else {
                        llSay(0, "Cannot remove host from the match.");
                    }
                }
            }

        } else if (chan == gChanGlobal) {
            if (gClientNum < gClientMax) {
                list temp = llParseString2List(message, ["|"], [""]);
                string action = llList2String(temp, 0);
                if (action == "FIND MATCH") {
                    integer chanResponse = llList2Integer(temp, 2);
                    llRegionSay(chanResponse, "OPEN MATCH|" + (string)gOwner + "|" + (string)gChanMatch);
                }
            }
        }
    }

    link_message(integer source, integer num, string str, key id)
    {
        if (id == gMultiplayer) {
            list temp = llParseString2List(str, ["|"], [""]);
            string action = llList2String(temp, 0);
            if (action == "Close Match") {
                llSay(0, "Closing host connection.");
                llRegionSay(gChanMatch, "CLOSE MATCH");
                llListenRemove(gHandleMatch); llListenRemove(gChanGlobal);
                gHost = "";
                gJoinedMatch = FALSE;
                state default;
            } else if (action == "Join Match") {
                llSay(0, "Closing host connection.");
                llRegionSay(gChanMatch, "CLOSE MATCH");
                llListenRemove(gHandleMatch); llListenRemove(gChanGlobal);
                gHost = "";
                gJoinedMatch = FALSE;
                state client_setup;
            }
        }
    }
}

state client_setup
{
    state_entry()
    {
        gChanMatch = -1 - (integer)((llFrand(1000) + (llFrand(1000) / 2)));
        gPlayerList = [];
        gJoinedMatch = FALSE;
        gHandleGlobal = llListen(gChanGlobal, "", "", "");
    }

    link_message(integer source, integer num, string str, key id)
    {
        if (id == gMultiplayer) {
            list temp = llParseString2List(str, ["|"], [""]);
            string action = llList2String(temp, 0);

            if (gJoinedMatch == FALSE && action == "Join Match") {
                if (gQuery == TRUE) {
                    llListenRemove(gHandleQuery); llListenRemove(gChanGlobal);
                    key body = llList2String(temp, 1);
                    integer i = llListFindList(gMatchList, [body]);
                    integer chanResponse = llList2Integer(gMatchList, i + 1);
                    llRegionSay(chanResponse, "JOIN|" + (string)gOwner + "|" + (string)gChanQuery);
                } else {
                        llSay(0, "Match has not been selected. Please select a match to join.");
                }
            }
            else if (gJoinedMatch == FALSE && action == "Find Match") {
                gQuery = TRUE;
                gHandleQuery = llListen(gChanQuery, "", "", "");
                llRegionSay(gChanGlobal, "FIND MATCH|" + (string)gOwner + (string)gChanQuery);
            }
            else if (gJoinedMatch == TRUE && action == "Leave Match") {
                gChanMatch = -1 - (integer)((llFrand(1000) + (llFrand(1000) / 2)));
                gJoinedMatch = FALSE;
                gHandleGlobal = llListen(gChanGlobal, "", "", "");
            }
        }
    }

    timer()
    {
            list buttons;
            integer length = llGetListLength(gMatchList);
            integer index;
            for (index = 0; index < length; index += 2) {
                buttons = buttons + [llList2String(gMatchList, index)];
            }
            llMessageLinked(LINK_THIS, 0, "MATCH LIST|" + llList2CSV(buttons), gDialog);
            llSay(0, "MATCH LIST|" + (string)buttons);
            llSetTimerEvent(0);
    }

    listen(integer chan, string name, key id, string message)
    {
        list temp = llParseString2List(message, ["|"], [""]);
        string action = llList2String(temp, 0);
        if (chan == gChanQuery) {
            if (action == "OPEN MATCH") {
                string matchName = llKey2Name(llList2String(temp,1));
                integer chanResponse = llList2Integer(temp,2);
                gMatchList = gMatchList + [matchName] + [chanResponse];
                llSetTimerEvent(3);
            } else if (action == "NO ROOM") {
                string matchName = llKey2Name(llList2String(temp,1));
                integer i = llListFindList(gMatchList, [matchName]);
                if (i != -1) {
                    gMatchList = llDeleteSubList(gMatchList, i, i);
                    llSay(0, "Join failure, no more open slots.");
                    llSetTimerEvent(1);
                }
            }

        } else if (chan == gChanMatch) {
            if (action == "CLOSE MATCH") {
                llListenRemove(gChanMatch);
                gChanMatch = -1 - (integer)((llFrand(1000) + (llFrand(1000) / 2)));
                gJoinedMatch = FALSE;
                gHandleGlobal = llListen(gChanGlobal, "", "", "");
            } else if (action == "START MATCH") {
                //state match_start;
                llSay(0,"Match start detected from host");
            }
        }
    }
}